from sklearn import datasets
from sklearn import svm
import numpy as np


class Selector:

    def __init__(self, *indices):
        self.indices = indices


class WinePredictor:

    def __init__(self):
        wine = datasets.load_wine()
        self.clf = svm.SVC(gamma=0.001, C=100.0)
        self.X, self.y = wine.data, wine.target
        self.prediction = []

    def fit(self, selector: Selector):
        Xs = self.X[0:0]
        ys = self.y[0:0]
        for i0, i1 in selector.indices:
            ys = np.append(ys, self.y[i0: i1])
            Xs = np.append(Xs, self.X[i0: i1], axis=0)
        self.clf.fit(Xs, ys)

    def predict(self):
        self.prediction = self.clf.predict(self.X)
        return self.prediction, self.errors()

    def errors(self):
        return [i for i, b in enumerate(self.prediction == self.y) if not b]


if __name__ == '__main__':
    wine = WinePredictor()
    wine.fit(Selector((0, 58), (59, 129), (130, 176)))
    P, e = wine.predict()
    print(P, wine.y, e)



