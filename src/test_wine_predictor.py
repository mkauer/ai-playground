from unittest import TestCase
import numpy as np

from src.main import WinePredictor, Selector


class TestWinePredictor(TestCase):

    def test_wrong_prediction(self):
        wine = WinePredictor()
        wine.fit(Selector((0, 50), (59, 120), (130, 170)))
        P, e = wine.predict()
        self.assertTrue(0 < len(e) < 10)

    def test_correct_prediction(self):
        wine = WinePredictor()
        wine.fit(Selector((0, 58), (59, 129), (130, 176)))
        P, e = wine.predict()
        self.assertEqual(0, len(e))
        self.assertTrue(np.array_equal(P, wine.y))
